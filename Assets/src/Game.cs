﻿using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour 
{
	public GameObject table;
	
	void Start () 
	{
		Screen.orientation = ScreenOrientation.LandscapeLeft;
	}
	
	void Update () 
	{
	
	}
	
	void OnGUI()
	{
		if (GUI.Button(new Rect(Screen.width - 80, Screen.height - 60, 70, 50),"Restart"))
		{
			table.GetComponent<Table>().restart();
		}
	}
}
