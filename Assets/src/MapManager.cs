﻿using UnityEngine;
using System.Collections;

public class MapManager
{
	public MapManager()
	{
	}
	
	public int[,,] getMap()
	{
		return new int[,,] {
			{
				{0, 0, 0, 0, 0, 0, 0},
				{0, 0, 4, 0, 0, 0, 0},
				{0, 4, 4, 0, 2, 0, 0},
				{0, 0, 0, 0, 2, 0, 0},
				{0, 0, 2, 2, 3, 0, 3},
				{0, 0, 0, 0, 0, 3, 0},
				{0, 0, 0, 0, 3, 0, 1}
			},
			{
				{0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 2, 0, 0},
				{0, 0, 0, 0, 0, 0, 0},
				{0, 0, 2, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 1}
			},
			{
				{0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 1}
			}
		};
	}
}
