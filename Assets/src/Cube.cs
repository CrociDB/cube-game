﻿using UnityEngine;
using System.Collections;

public class Cube : MonoBehaviour 
{
	public static readonly int TOTAL_TYPES = 4;
	public static readonly float ANIM_TIME = .07f;
	public static readonly float EXPLODE_TIME = .4f;
	
	private CubeType type;
	private bool marked;
	
	public int Type
	{
		set 
		{
			type = (CubeType) value;
			changeColor();
		}
		get
		{
			return (int) type;
		}
	}
	
	public bool Marked
	{
		get
		{
			return marked;
		}
	}
	
	public void mark()
	{
		marked = true;
	}
	
	public void unmark()
	{
		marked = false;
	}
	
	private void changeColor()
	{
		renderer.material.shader = Shader.Find ("VertexLit");
		switch(type)
		{
			case CubeType.Blue:
				renderer.material.SetColor ("_Color", Color.blue);
				break;
			case CubeType.Red:
				renderer.material.SetColor ("_Color", Color.red);
				break;
			case CubeType.Yellow:
				renderer.material.SetColor ("_Color", Color.yellow);
				break;
			case CubeType.Green:
				renderer.material.SetColor ("_Color", Color.green);
				break;
		}
	}
	
	public void kill()
	{
		iTween.ScaleTo(gameObject, Vector3.zero, EXPLODE_TIME);
		iTween.ColorTo(gameObject, iTween.Hash(
									"a", 0f, 
									"r", 0f, 
									"g", 0f, 
									"b", 0f, 
									"time", EXPLODE_TIME));
		Invoke("destroy", EXPLODE_TIME + .1f);
	}
	
	private void destroy()
	{
		Destroy(gameObject);
	}
	
	void Start ()
	{
		marked = false;
	}
	
	void Update () 
	{
	
	}
}

enum CubeType
{
	Blue = 1,
	Red,
	Yellow,
	Green
}