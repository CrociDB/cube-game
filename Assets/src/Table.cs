﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Table : MonoBehaviour 
{
	private static readonly float GUI_OBJECT_SCALE = .6f;
	private static readonly float GUI_OBJECT_OFFSET = .2f;
	
	public GameObject guiObject;
	
	public int matrixSize;
	public GameObject prefabCube;
	public GameObject prefabPositionClick;
	
	private GameObject tableBase;
	private MapManager mapManager;
	private CubeMap map;
	
	private Queue<int> nextCubeQueue;
	private GameObject nextCube;
	private PositionClick currentPosition;
	
	private GameState state;
	
	void Start () 
	{
		init();
	}
	
	private void init()
	{
		state = new GameState();
		
		mapManager = new MapManager();
		
		GameObject map_logic = new GameObject();
		map_logic.AddComponent<CubeMap>();
		map_logic.transform.parent = transform;
		map = map_logic.GetComponent<CubeMap>();
		map.init(matrixSize);
			
		createTableBase();
		loadMap();
		createNextCubeList();
		
		getCube();
		state.turn();
	}
	
	public void restart()
	{
		Destroy(tableBase);
		Destroy(nextCube);
		
		foreach (Transform t in guiObject.transform)
		{
			Destroy(t.gameObject);
		}
		
		init();
	}
	
	private void createTableBase()
	{
		tableBase = new GameObject();
		tableBase.name = "TableItems";
		
		for (int i = 0; i < matrixSize; i++)
		{
			for (int j = 0; j < matrixSize; j++)
			{
	            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
	            cube.transform.position = new Vector3(i, 0, j);
				cube.transform.localScale = new Vector3(1, .2f, 1);
				cube.transform.parent = tableBase.transform;
				
				cube.renderer.material.shader = Shader.Find ("VertexLit");
				if ((i%2 == 0 && j%2 == 0) || (i%2 == 1 && j%2 == 1)) cube.renderer.material.SetColor ("_Color", Color.white);
				else cube.renderer.material.SetColor ("_Color", Color.gray);
				
				map.addBase(i, j, ref cube);
			}
		}
		
		// Create event gameobjects
		for (int i = 0; i < matrixSize; i++)
		{
			// X
			GameObject evt = (GameObject) Instantiate(prefabPositionClick);
			evt.GetComponent<PositionClick>().x = true;
			evt.GetComponent<PositionClick>().index = i;
            evt.transform.position = new Vector3(i, 0, -1);
			evt.transform.localScale = new Vector3(1, .2f, 5);
			evt.transform.parent = tableBase.transform;
			
			evt.transform.name = "PositionClick";
			evt.GetComponent<MeshRenderer>().enabled = false;
			
			// Y
			evt = (GameObject) Instantiate(prefabPositionClick);
			evt.GetComponent<PositionClick>().x = false;
			evt.GetComponent<PositionClick>().index = i;
            evt.transform.position = new Vector3(-1, 0, i);
			evt.transform.localScale = new Vector3(5, .2f, 1);
			evt.transform.parent = tableBase.transform;
			
			evt.transform.name = "PositionClick";
			evt.GetComponent<MeshRenderer>().enabled = false;
			
			currentPosition = evt.GetComponent<PositionClick>();
		}
		
		tableBase.transform.position = new Vector3(-3, 0, -3);
		tableBase.transform.parent = this.transform;
	}
	
	private void loadMap()
	{
		int[,,] mapToLoad = mapManager.getMap();
		
		for (int level = 0; level < 3; level++)
		{
			for (int i = 0; i < matrixSize; i++)
			{
				for (int j = 0; j < matrixSize; j++)
				{
					if (mapToLoad[level,i,j] == 0) continue;
					
					GameObject cube = (GameObject)Instantiate(prefabCube);
					cube.transform.name = "Cube";
					cube.GetComponent<Cube>().Type = mapToLoad[level,i,j];
					cube.transform.parent = tableBase.transform;
					cube.transform.position = new Vector3(
									currentPosition.transform.position.x,
									.6f,
									currentPosition.transform.position.z);
					
					map.simplyAdd(i, j, level, cube);
				}
			}
		}
	}
	
	private void createNextCubeList()
	{
		nextCubeQueue = new Queue<int>();
		
		for (int i = 0; i < 3; i++) generateNewCube();
	}
	
	private void generateNewCube()
	{
		nextCubeQueue.Enqueue(Random.Range(0, Cube.TOTAL_TYPES) + 1);
		updateGUIObject();
	}
	
	private int getCubeFromList()
	{
		int cubeNum = nextCubeQueue.Dequeue();
		generateNewCube();
		return cubeNum;
	}
	
	private void getCube()
	{
		nextCube = (GameObject)Instantiate(prefabCube);
		nextCube.transform.name = "Cube";
		nextCube.GetComponent<Cube>().Type = getCubeFromList();
		nextCube.transform.parent = tableBase.transform;
		nextCube.transform.position = new Vector3(
						currentPosition.transform.position.x,
						.6f,
						currentPosition.transform.position.z);
	}
	
	private void throwCube()
	{
		bool added;
		
		if (currentPosition.x)
		{
			added = map.addX(nextCube, currentPosition.index);
		}
		else
		{
			added = map.addY(nextCube, currentPosition.index);
		}
		
		if (added)
		{
			state.wait();
		}
		else
		{
			Debug.Log("Oops, you can't do that");
		}
	}
	
	public void finishedTurn()
	{
		state.turn();
		getCube();
	}
	
	void Update () 
	{
		if (state.State == GameStateValues.TURN)
		{
			if (Input.GetMouseButton(0))
			{
				RaycastHit hit;
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				if (Physics.Raycast (ray, out hit, 100.0f))
				{
					GameObject obj = hit.collider.gameObject;
					if (obj.transform.name == "PositionClick")
					{
						nextCube.transform.position = new Vector3(
							obj.transform.position.x,
							.6f,
							obj.transform.position.z);
						
						currentPosition = obj.GetComponent<PositionClick>();
					}
				}
			}
			
			if (Input.GetMouseButtonDown(0))
			{
				RaycastHit hit;
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				if (Physics.Raycast (ray, out hit, 100.0f))
				{
					GameObject obj = hit.collider.gameObject;
					if (obj.transform.name == "Cube")
					{
						throwCube();
					}
				}
			}
		}
	}
	
	private void updateGUIObject()
	{
		foreach (Transform t in guiObject.transform)
		{
			Destroy(t.gameObject);
		}
		
		int count = nextCubeQueue.Count - 1;
		foreach (int i in nextCubeQueue)
		{
			GameObject guiNextCube = (GameObject) Instantiate(prefabCube);
			guiNextCube.transform.parent = guiObject.transform;
			
			if (count == nextCubeQueue.Count - 1)
			{
				guiNextCube.transform.localScale = guiNextCube.transform.localScale * (GUI_OBJECT_SCALE + .2f);
								guiNextCube.transform.localPosition = new Vector3(
					count * (guiNextCube.transform.localScale.x + GUI_OBJECT_OFFSET - .15f), 
					0, 
					-count * (guiNextCube.transform.localScale.z + GUI_OBJECT_OFFSET - .15f));
			}
			else
			{
				guiNextCube.transform.localScale = guiNextCube.transform.localScale * GUI_OBJECT_SCALE;
				guiNextCube.transform.localPosition = new Vector3(
					count * (guiNextCube.transform.localScale.x + GUI_OBJECT_OFFSET), 
					0, 
					-count * (guiNextCube.transform.localScale.z + GUI_OBJECT_OFFSET));
			}
			
			
			guiNextCube.GetComponent<Cube>().Type = i;
			
			count--;
		}
	}
}

enum GameStateValues
{
	LOADING,
	WAITING,
	TURN
}

class GameState
{
	private GameStateValues state;
	
	public GameStateValues State
	{
		get
		{
			return state;
		}
	}
	
	public GameState()
	{
		state = GameStateValues.LOADING;
	}
	
	public void wait()
	{
		state = GameStateValues.WAITING;
	}
	
	public void turn()
	{
		state = GameStateValues.TURN;
	}
}
