﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CubeMap : MonoBehaviour
{
	private int size;
	private CubeSlot[,] map;
	private List<Cube> cubeList;
	
	private List<Cube> cubesToKill;
	
	void Start () 
	{
	}
	
	void Update ()
	{
	}
	
	public void init(int size)
	{
		this.size = size;
		
		map = new CubeSlot[size,size];
		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j < size; j++)
			{
				map[i,j] = new CubeSlot();
			}
		}
		
		cubeList = new List<Cube>();
	}
	
	public void addBase(int x, int y, ref GameObject b)
	{
		map[x,y].baseFloor = b;
	}
	
	public void simplyAdd(int x, int y, int level, GameObject cube)
	{
		cubeList.Add(cube.GetComponent<Cube>());
		
		map[x,y].cube[level] = cube;
		map[x,y].running = false;
		map[x,y].x = false;
		
		cube.transform.position = new Vector3(
			map[x,y].baseFloor.transform.position.x, 
			map[x,y].cube[level].transform.position.y + (level * map[x,y].cube[level].transform.localScale.y),
			map[x,y].baseFloor.transform.position.z);
	}
	
	public void moveBlockTo(int x1, int y1, int x2, int y2)
	{
		map[x2,y2].cube[0] = map[x1,y1].cube[0];
		map[x2,y2].running = map[x1,y1].running;
		map[x2,y2].x = map[x1,y1].x;
		
		map[x1,y1].cube[0] = null;
		map[x1,y1].running = false;
		map[x1,y1].x = false;
	}
	
	public bool addX(GameObject cube, int index)
	{
		bool moved = false;
		
		if (!map[index,0].isEmpty()) 
		{
			if (map[index,1].isEmpty())
			{
				moveBlockTo(index, 0, index, 1);
				simplyMoveCube(index, 1);
				moved = true;
			}
			else
			{
				return false;
			}
		}
		
		cubeList.Add(cube.GetComponent<Cube>());
		
		map[index,0].cube[0] = cube;
		map[index,0].running = !moved;
		map[index,0].x = true;
		
		moveCube(index, 0);
		
		return true;
	}
	
	public bool addY(GameObject cube, int index)
	{		
		bool moved = false;
		
		if (!map[0,index].isEmpty()) 
		{
			if (map[1,index].isEmpty())
			{
				moveBlockTo(0, index, 1, index);
				simplyMoveCube(1, index);
				moved = true;
			}
			else
			{
				return false;
			}
		}
		
		cubeList.Add(cube.GetComponent<Cube>());
		
		map[0,index].cube[0] = cube;
		map[0,index].running = !moved;
		map[0,index].x = false;
		
		moveCube(0, index);
		
		return true;
	}
	
	private void moveCube(int x, int y)
	{
		Vector3 pos = new Vector3(
			map[x,y].baseFloor.transform.position.x, 
			map[x,y].cube[0].transform.position.y,
			map[x,y].baseFloor.transform.position.z);
		
		iTween.MoveTo(map[x,y].cube[0], iTween.Hash(new object[] { 
													"position", pos, 
													"time", Cube.ANIM_TIME, 
													"easetype", "linear", 
													"oncompletetarget", this.gameObject,
													"oncomplete", "updateCube", 
													"oncompleteparams", new Vector2(x, y) }));
	}
	
	private void simplyMoveCube(int x, int y)
	{
		Vector3 pos = new Vector3(
			map[x,y].baseFloor.transform.position.x, 
			map[x,y].cube[0].transform.position.y,
			map[x,y].baseFloor.transform.position.z);
		
		iTween.MoveTo(map[x,y].cube[0], iTween.Hash(new object[] { 
													"position", pos, 
													"time", Cube.ANIM_TIME, 
													"easetype", "linear",
													"oncompletetarget", this.gameObject,
													"oncomplete", "checkMatchVec2d",
													"oncompleteparams", new Vector2(x,y) }));
	}
	
	private void updateCube(Vector2 pos)
	{
		int x = (int) pos.x;
		int y = (int) pos.y;
		
		if (!map[x,y].running) return;
		
		if (!map[x,y].x)
		{
			sendX(x,y);
		}
		else
		{
			sendY(x,y);
		}
	}
	
	private bool sendX(int x, int y)
	{ 
		bool moved = false;
		
		if (x + 1 >= size) 
		{
			map[x,y].running = false;
			checkMatch(x,y);
			return false;
		}
		
		if (!map[x+1,y].isEmpty())
		{
			if (x + 2 >= size)
			{
				map[x,y].running = false;
				checkMatch(x,y);
				return false;
			}
			else
			{
				if (map[x+2,y].isEmpty())
				{
					moveBlockTo(x+1, y, x+2, y);
					simplyMoveCube(x+2,y);
					moved = true;
				}
				else
				{
					map[x,y].running = false;
					checkMatch(x,y);
					return false;
				}
			}
		}
		
		map[x+1,y].cube[0] = map[x,y].cube[0];
		map[x+1,y].running = !(moved || !map[x,y].running);
		map[x+1,y].x = map[x,y].x;
		
		map[x,y].cube[0] = null;
		map[x,y].running = false;
		map[x,y].x = false;
		
		moveCube(x+1, y);
		
		return true;
	}
	
	private bool sendY(int x, int y)
	{
		bool moved = false;
		
		if (y + 1 >= size) 
		{
			map[x,y].running = false;
			checkMatch(x,y);
			return false;
		}
		
		if (!map[x,y+1].isEmpty())
		{
			if (y + 2 >= size)
			{
				map[x,y].running = false;
				checkMatch(x,y);
				return false;
			}
			else
			{
				if (map[x,y+2].isEmpty())
				{
					moveBlockTo(x, y+1, x, y + 2);
					simplyMoveCube(x,y+2);
					moved = true;
				}
				else
				{
					map[x,y].running = false;
					checkMatch(x,y);
					return false;
				}
			}
		}
		
		map[x,y+1].cube[0] = map[x,y].cube[0];
		map[x,y+1].running = !(moved || !map[x,y].running);
		map[x,y+1].x = map[x,y].x;
		
		map[x,y].cube[0] = null;
		map[x,y].running = false;
		map[x,y].x = false;
		
		moveCube(x, y+1);
		
		return true;
	}
	
	private void checkMatchVec2d(Vector2 vec2d)
	{
		checkMatch((int) vec2d.x, (int) vec2d.y);
	}
	
	private void checkMatch(int x, int y)
	{
		unmarkAll();
		
		cubesToKill = new List<Cube>();
		startChecking(x, y, 0, map[x,y].cube[0].GetComponent<Cube>().Type);
		
		if (cubesToKill.Count >= 3)
		{
			killCubes();
		}
		
		SendMessageUpwards("finishedTurn");
	}
	
	private void startChecking(int x, int y, int level, int type)
	{
		if (x >= size || x < 0) return;
		if (y >= size || y < 0) return;
		if (level >= 3 || level < 0) return;
		if (map[x,y].isEmpty(level)) return;
		if (map[x,y].cube[level].GetComponent<Cube>().Type != type) return;
		if (map[x,y].cube[level].GetComponent<Cube>().Marked) return;
		
		map[x,y].cube[level].GetComponent<Cube>().mark();
		cubesToKill.Add(map[x,y].cube[level].GetComponent<Cube>());
		
		startChecking(x, y, level+1, type);
		startChecking(x, y, level-1, type);
		startChecking(x+1, y, level, type);
		startChecking(x-1, y, level, type);
		startChecking(x, y+1, level, type);
		startChecking(x, y-1, level, type);
	}
	
	private void killCubes()
	{
		foreach (Cube c in cubesToKill)
		{
			cubeList.Remove(c.GetComponent<Cube>());
			c.GetComponent<Cube>().kill();
		}
		
		
	}
	
	private void unmarkAll()
	{
		foreach (Cube c in cubeList)
		{
			c.unmark();
		}
	}
}

class CubeSlot
{
	public GameObject baseFloor;
	public GameObject[] cube;
	public bool running;
	public bool x;
	
	public CubeSlot()
	{
		cube = new GameObject[3];
	}
	
	public bool isEmpty()
	{
		return isEmpty(0);
	}
	
	public bool isEmpty(int level)
	{
		return cube[level] == null;
	}
	
	public void reset()
	{
		cube[0] = null;
		running = false;
		x = false;
	}
	
	override public string ToString()
	{
		return "[Cube Slot baseFloor=" + baseFloor + ", cube=" + cube + ", running=" + running + ", x=" + x + "]";
	}
}
